from os import path
from pathlib import Path
from flask import Flask, render_template, send_from_directory
from flask_frozen import Freezer

template_folder = path.abspath('./wiki')
app = Flask(__name__, template_folder=template_folder)

# Configuration for Flask-Frozen
app.config['FREEZER_DESTINATION'] = 'public'
app.config['FREEZER_RELATIVE_URLS'] = True
app.config['FREEZER_IGNORE_MIMETYPE_WARNINGS'] = True

freezer = Freezer(app)

@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()

@app.route('/favicon.ico') 
def favicon():
    return send_from_directory(str(Path('static')) + '/assets', 'gem.png', mimetype='image/vnd.microsoft.icon')

@app.route('/')
def home():
    return render_template('pages/home.html')

@app.route('/<page>')
def pages(page):
    return render_template(str(Path('pages')) + '/' + page.lower() + '.html')

# Additional route to serve static files
@app.route('/static/<path:filename>')
def static_files(filename):
    return send_from_directory('static', filename)

if __name__ == "__main__":
    app.run(port=8080)
